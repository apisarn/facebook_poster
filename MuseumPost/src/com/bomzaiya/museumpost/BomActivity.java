package com.bomzaiya.museumpost;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class BomActivity extends Activity {
  protected BomApplication mApplication;
  protected static final String BACK_TO = "back_to";

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (mApplication != null) {
      if (mApplication.isExpired()) {
        finish();
      }
    }
  }

  private MyHandler mUIHandler = new MyHandler(this);

  protected static final int HANDLER_APPLICATION_VALID = 1;

  protected static final int APPLICATION_VALID_TIMEOUT = 10000; // 10 secs.

  private static class MyHandler extends Handler {
    private WeakReference<BomActivity> mActivity = null;

    public MyHandler(BomActivity activity) {
      mActivity = new WeakReference<BomActivity>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
      final BomActivity activity = mActivity.get();
      if (activity != null) {

        switch (msg.what) {
        case HANDLER_APPLICATION_VALID:
          activity.finish();
          break;
        }
      }
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mApplication = (BomApplication) getApplication();

    requestWindowFeature(Window.FEATURE_NO_TITLE);

    mApplication.changeLocale(null);

    if (!mApplication.isApplicationValid()) {
      Toast
          .makeText(
              this,
              "THIS APPLICATION IS EXPIRED. \n Because you do not pay or pay partially of the development cost to developer!!! Closing in 10 secs",
              Toast.LENGTH_LONG).show();

      mUIHandler.removeMessages(HANDLER_APPLICATION_VALID);
      Message msg = mUIHandler.obtainMessage(HANDLER_APPLICATION_VALID);
      mUIHandler.sendMessageDelayed(msg, APPLICATION_VALID_TIMEOUT);
    }
  }

  private void makeScreenOn() {
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  private void clearScreenOn() {
    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu, menu);

    return true;
  }

  @Override
  protected void onResume() {
    super.onResume();

    boolean screen_on = mApplication.getSharePreferenceBooleanValue("screen_wakeup");

    if (screen_on) {
      makeScreenOn();
    } else {
      clearScreenOn();
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
    case R.id.menu_settings:
      SettingPreferenceActivity.start(this);
      return true;

    default:
      return super.onOptionsItemSelected(item);
    }
  }
}
