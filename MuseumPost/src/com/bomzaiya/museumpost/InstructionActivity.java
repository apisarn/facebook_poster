package com.bomzaiya.museumpost;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class InstructionActivity extends BomActivity {
  protected static final int MAIN_INSTRUCTION = 1888;
  public static final String MUSEUM_FOLDER = Environment
      .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
      + "/nidamuseum";
  public static final String FRAME_FOLDER = MUSEUM_FOLDER + "/frame";

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
      case LikeActivity.LIKE_MUSEUM:
        String backTo = data.getStringExtra(BACK_TO);
        if (!getClass().getName().equals(backTo)) {
          setResult(Activity.RESULT_OK, data);
          finish();
        }
        break;

      default:
        break;
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  public static void startForResult(BomActivity activity) {
    Intent intent = new Intent(activity, InstructionActivity.class);
    activity.startActivityForResult(intent, MAIN_INSTRUCTION);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.instruction);

    Button buttonBack = (Button) findViewById(R.id.buttonBack);
    buttonBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
      }
    });

    Button buttonChooseFrame = (Button) findViewById(R.id.buttonChooseFrame);
    buttonChooseFrame.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        LikeActivity.startForResult(InstructionActivity.this);
      }
    });

  }

}
