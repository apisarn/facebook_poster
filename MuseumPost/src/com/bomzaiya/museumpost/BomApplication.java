package com.bomzaiya.museumpost;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.bomzaiya.museumpost.media.OnFacebook;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.internal.SessionTracker;
import com.facebook.internal.Utility;

//httpMethod = "POST",
@ReportsCrashes(formKey = "", formUri = "http://bom.linegig.com/bug/arca.php", formUriBasicAuthLogin = "fingi", formUriBasicAuthPassword = "fingi", customReportContent = {
    ReportField.APP_VERSION_CODE, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.DEVICE_ID,
    ReportField.STACK_TRACE, ReportField.LOGCAT }, mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text)
public class BomApplication extends Application {
  public static final String PREFERENCES = "PREFERENCES";
  public static final int PREFERENCE_MODE = 0;
  private Session mReadSession;
  private Session mPublishSession;
  private SessionTracker mSessionTracker;

  public static String PAGE_ACCESS_TOKEN = "AAAHeIYULUekBALd6FkZAsWMDg96CeqMHQosTvL9aRiIu23AH8lXr7alrUhH7CiDLUnp3nsrh3RDQTQ2ZCkMntGICc0i3p3LZBZBHZBS7Bb9xOmVG2La0i";
  public static String PAGE_ACCESS_TOKEN2 = "AAAHeIYULUekBAKHJMZCzWNqu93e0jTx63D9ZAhEPTX0xyNo7ilXMWJZCKdBPIMaotbsiSZCcU6WORHTcFe7fQMGAGdPmGwy2K6weAxalhP7oOXwnXGZCn";
  private String mPageAccessToken = "";
  private Locale mLocale;
  private boolean mExpired = false;

  @Override
  public void onCreate() {
    ACRA.init(this);
    ACRA.getErrorReporter().putCustomData("IMEI", getPhoneIMEI());
    ACRA.getErrorReporter().putCustomData("MAC", getPhoneMAC());
    // ACRA.getErrorReporter().putCustomData("FINGI_APP_NAME",
    // ProductConfig.APP_NAME);
    // ACRA.getErrorReporter().putCustomData("FINGI_VERSION_NAME",
    // ProductConfig.PRODUCT_CONFIG_NAME);
    super.onCreate();

    mSessionTracker = new SessionTracker(getBaseContext(), new StatusCallback() {

      @Override
      public void call(Session session, SessionState state, Exception exception) {
      }
    }, null, false);

    String applicationId = Utility.getMetadataApplicationId(getBaseContext());
    mReadSession = mSessionTracker.getSession();
  }

  public String getPhoneIMEI() {
    // get imsi (sim) and imei (phone) codes, for the asterisk integration
    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    return telephonyManager.getDeviceId();
  }

  public String getPhoneMAC() {
    WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    WifiInfo wifiInf = wifiManager.getConnectionInfo();
    return wifiInf.getMacAddress();
  }

  public void getFacebookReadSession(Activity activity, final OnFacebook onFacebook) {

    boolean getnew = false;
    if (mReadSession == null || mReadSession.getState().isClosed() || mReadSession.getAccessToken() == null) {
      getnew = true;

    } else if (mReadSession.getAccessToken().equals("")) {
      // String token = mReadSession.getAccessToken();
      getnew = true;
    }

    if (getnew) {
      openActiveReadSession(activity, true, new StatusCallback() {

        @Override
        public void call(Session session, SessionState state, Exception exception) {
          mReadSession = session;
          String token = mReadSession.getAccessToken();
          onFacebook.onSuccess(session);
          return;
        }
      }, Arrays.asList("user_birthday", "email", "user_location", "user_events"));
    } else {
      onFacebook.onSuccess(mReadSession);
    }
    return;
  }

  public void getFacebookPublishSession(Activity activity, final OnFacebook onFacebook) {
    boolean getnew = false;
    if (mPublishSession == null || mPublishSession.getState().isClosed() || mPublishSession.getAccessToken() == null) {
      getnew = true;

    } else if (mPublishSession.getAccessToken().equals("")) {
      // String token = mReadSession.getAccessToken();
      getnew = true;
    }
    if (getnew) {
      openActivePublishSession(activity, true, new StatusCallback() {

        @Override
        public void call(Session session, SessionState state, Exception exception) {
          mPublishSession = session;
          onFacebook.onSuccess(session);
          return;
        }
      }, Arrays.asList("publish_actions", "publish_stream", "manage_pages"));
    } else {
      onFacebook.onSuccess(mPublishSession);
    }
    return;
  }

  private static Session openActiveReadSession(Activity activity, boolean allowLoginUI, StatusCallback callback,
      List<String> readPermissions) {
    OpenRequest openRequest = new OpenRequest(activity).setPermissions(readPermissions).setCallback(callback);
    // Session session = new Session.Builder(activity).build();
    Session.Builder sessionBuilder = new Session.Builder(activity);
    sessionBuilder.setApplicationId(ProductConfig.FACEBOOK_APP_ID);

    Session session = sessionBuilder.build();
    if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
      Session.setActiveSession(session);
      session.openForRead(openRequest);
      return session;
    }
    return null;
  }

  private static Session openActivePublishSession(Activity activity, boolean allowLoginUI, StatusCallback callback,
      List<String> publishPermissions) {
    OpenRequest openRequest = new OpenRequest(activity).setPermissions(publishPermissions).setCallback(callback);
    // Session session = new Session.Builder(activity).build();
    Session.Builder sessionBuilder = new Session.Builder(activity);
    sessionBuilder.setApplicationId(ProductConfig.FACEBOOK_APP_ID);

    Session session = sessionBuilder.build();
    if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
      Session.setActiveSession(session);
      session.openForPublish(openRequest);
      return session;
    }
    return null;
  }

  public boolean isTablet() {
    boolean xlarge = ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
    boolean large = ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
    return (xlarge || large);
  }

  public void setPageAccessToken(String access_token) {
    mPageAccessToken = access_token;
  }

  public String getPageAccessToken() {
    return mPageAccessToken;
  }

  public boolean hasInternet() {
    ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

    boolean hasNetwork = false;
    if (networkInfo != null) {
      if (networkInfo.getType() != -1) {
        hasNetwork = true;
      }
    }
    return hasNetwork;
  }

  public void changeLocale(String localeText) {
    Locale locale = null;
    if (localeText == null || localeText.equals("")) {
      if (mLocale == null) {
        return;
      } else {
        locale = mLocale;
      }
    } else {
      locale = new Locale(localeText);
    }

    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getResources().updateConfiguration(config, getResources().getDisplayMetrics());
  }

  /**
   * set shared preference boolean value
   * 
   * @param prefId
   * @param prefValue
   */
  public void setSharePreferenceBooleanValue(String prefId, boolean prefValue) {
    SharedPreferences pref = getSharedPreferences(PREFERENCES, PREFERENCE_MODE);
    SharedPreferences.Editor editor = pref.edit();

    editor.putBoolean(prefId, prefValue);
    editor.commit();
  }

  public boolean getSharePreferenceBooleanValue(String prefId) {
    SharedPreferences sharePrefs = getSharedPreferences(PREFERENCES, PREFERENCE_MODE);
    return sharePrefs.getBoolean(prefId, false);
  }

  public void setExpired(boolean b) {
    mExpired = b;
  }

  public boolean isExpired() {
    return mExpired;
  }

  @SuppressWarnings("unused")
  public boolean isApplicationValid() {
    if (ProductConfig.REACH_MAXIMUM_USED == 0) {
      setExpired(false);
      return true;
    } else {
      StatDatabaseAdapter db = new StatDatabaseAdapter(this);
      Cursor cursor = db.getLikeDislikeCount();
      if (cursor != null) {
        if (cursor.getCount() > 0) {
          cursor.moveToFirst();
          int likeCount = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUME_LIKE_COUNT));
          int dislikeCount = cursor.getInt(cursor.getColumnIndexOrThrow(StatDatabaseAdapter.STATS_COLUME_DISLIKE_COUNT));

          if (likeCount + dislikeCount > ProductConfig.REACH_MAXIMUM_USED) {
            setExpired(true);
            return false;
          }
        }
        cursor.close();
      }
      db.close();
    }
    return true;
  }
}
