package com.bomzaiya.museumpost;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bomzaiya.museumpost.media.MediaUtility;
import com.bomzaiya.museumpost.media.OnFacebook;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;

public class PhotoPreviewActivity extends BomActivity {
  public static final int PHOTO_PREVIEW = 100;
  private static final String PREVIEW_FILE_NAME = "preview_file_name";
  private static String mPreviewFileName;
  private ProgressDialog mProgressDialog;

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    Session.getActiveSession().onActivityResult(this, requestCode, resultCode,
        data);
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
      case PostResultActivity.POST_RESULT:
        String backTo = data.getStringExtra(BACK_TO);
        if (!getClass().getName().equals(backTo)) {
          setResult(Activity.RESULT_OK, data);
          finish();
        }
        break;
      default:
        break;
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  public static void startForResult(BomActivity activity, String filename) {
    Intent intent = new Intent(activity, PhotoPreviewActivity.class);
    intent.putExtra(PREVIEW_FILE_NAME, filename);
    activity.startActivityForResult(intent, PHOTO_PREVIEW);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    // outState.put
    super.onSaveInstanceState(outState);
  }

  private MyHandler mUIHandler = new MyHandler(this);
  protected RequestAsyncTask mRequestPost;

  protected static final int HANDLER_POST_FACEBOOK = 1;

  protected static final int POST_FACEB00K_TIMEOUT = 90000; // 90 secs.

  private static class MyHandler extends Handler {
    private WeakReference<PhotoPreviewActivity> mActivity = null;

    public MyHandler(PhotoPreviewActivity activity) {
      mActivity = new WeakReference<PhotoPreviewActivity>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
      final PhotoPreviewActivity activity = mActivity.get();
      if (activity != null) {

        switch (msg.what) {
        case HANDLER_POST_FACEBOOK:
          if (activity.mProgressDialog != null) {
            activity.mProgressDialog.dismiss();
            activity.mRequestPost.cancel(true);

            PostResultActivity.startForResult(activity,
                PostResultActivity.FAIL,
                activity.getString(R.string.post_timeout));
          }
          break;
        }
      }
    }
  }

  private void postToFanPage(final Bitmap bi) {
    mProgressDialog = ProgressDialog.show(PhotoPreviewActivity.this,
        getString(R.string.uploading_title),
        getString(R.string.uploading_to_nida_fanpage), true, true);

    mUIHandler.removeMessages(HANDLER_POST_FACEBOOK);
    Message msg = mUIHandler.obtainMessage(HANDLER_POST_FACEBOOK);
    mUIHandler.sendMessageDelayed(msg, POST_FACEB00K_TIMEOUT);

    mApplication.getFacebookPublishSession(PhotoPreviewActivity.this,
        new OnFacebook() {

          @Override
          public void onSuccess(final Session session) {
            Bundle postParams = new Bundle();
            postParams.putString("message",
                getString(R.string.facebook_post_message));
            byte[] data = null;

            // Bitmap bi = BitmapFactory.decodeFile(xxx);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bi.compress(Bitmap.CompressFormat.PNG, 100, baos);

            data = baos.toByteArray();

            postParams.putString("access_token",
                mApplication.getPageAccessToken());

            // postParams.putString("access_token",
            // BomApplication.PAGE_ACCESS_TOKEN2);
            postParams.putByteArray("source", data);

            final Request.Callback callback = new Request.Callback() {
              public void onCompleted(Response response) {
                if (response.getGraphObject() != null) {
                  JSONObject graphResponse = response.getGraphObject()
                      .getInnerJSONObject();
                  String postId = null;
                  try {
                    postId = graphResponse.getString("id");
                  } catch (JSONException e) {
                    // Log.i("XXX", "JSON error " + e.getMessage());
                  }
                  FacebookRequestError error = response.getError();
                  mProgressDialog.dismiss();

                  mUIHandler.removeMessages(HANDLER_POST_FACEBOOK);
                  if (error != null) {
                    PostResultActivity.startForResult(
                        PhotoPreviewActivity.this, PostResultActivity.FAIL,
                        mPreviewFileName);
                  } else {
                    String msg = getString(R.string.see_posted_picture) + "\n"
                        + getString(R.string.facebook_url);
                    PostResultActivity.startForResult(
                        PhotoPreviewActivity.this, PostResultActivity.SUCCESS,
                        msg);
                  }
                }
              }
            };

            Request request = new Request(session,
                ProductConfig.FACEBOOK_PHOTO_GRAPH_PATH, postParams,
                HttpMethod.POST, callback);

            mRequestPost = new RequestAsyncTask(request);
            mRequestPost.execute();
          }
        });
  }

  /**
   * postTextToFanPage may use later
   */
  public void postTextToFanPage() {

    mApplication.getFacebookPublishSession(PhotoPreviewActivity.this,
        new OnFacebook() {

          @Override
          public void onSuccess(Session session) {
            Bundle postParams = new Bundle();
            postParams.putString("name", "Facebook SDK for Android");
            postParams.putString("caption",
                "Build great social apps and get more installs.");
            postParams
                .putString(
                    "message",
                    "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
            postParams.putString("link",
                "https://developers.facebook.com/android");
            postParams
                .putString("picture",
                    "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");
            postParams.putString("access_token", "");

            Request.Callback callback = new Request.Callback() {
              public void onCompleted(Response response) {
                if (response.getGraphObject() != null) {
                  JSONObject graphResponse = response.getGraphObject()
                      .getInnerJSONObject();
                  String postId = null;
                  try {
                    postId = graphResponse.getString("id");
                  } catch (JSONException e) {
                    // Log.i("XXX", "JSON error " + e.getMessage());
                  }
                  FacebookRequestError error = response.getError();
                  if (error != null) {
                    Toast.makeText(PhotoPreviewActivity.this,
                        error.getErrorMessage(), Toast.LENGTH_SHORT).show();
                  } else {
                    Toast.makeText(
                        PhotoPreviewActivity.this.getApplicationContext(),
                        postId, Toast.LENGTH_LONG).show();
                  }
                }
              }
            };

            Request request = new Request(session,
                ProductConfig.FACEBOOK_FEED_GRAPH_PATH, postParams,
                HttpMethod.POST, callback);

            RequestAsyncTask task = new RequestAsyncTask(request);
            task.execute();
          }
        });
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.photo_preview);

    Intent intent = getIntent();
    mPreviewFileName = intent.getStringExtra(PREVIEW_FILE_NAME);
  }

  @Override
  protected void onResume() {
    super.onResume();

    configurePage();
  }

  @Override
  protected void onPause() {
    super.onPause();

    if (mProgressDialog != null) {
      mProgressDialog.dismiss();
    }
  }

  private void configurePage() {
    ImageView ivPreview = (ImageView) findViewById(R.id.ivPreview);

    Drawable drawable = MediaUtility.readDrawable(getBaseContext(),
        mPreviewFileName);
    ivPreview.setImageDrawable(drawable);

    // Add a listener to the Capture button
    Button buttonSend = (Button) findViewById(R.id.buttonSend);
    buttonSend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext()
            .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        boolean canpost = false;
        if (networkInfo != null) {
          if (networkInfo.getType() != -1) {
            canpost = true;
          }
        }

        if (canpost) {
          Bitmap bi = MediaUtility.readBitmap(getBaseContext(),
              mPreviewFileName);
          postToFanPage(bi);
        } else {
          String msg = getString(R.string.connection_error);
          PostResultActivity.startForResult(PhotoPreviewActivity.this,
              PostResultActivity.FAIL, msg);
        }

      }
    });

    Button buttonBack = (Button) findViewById(R.id.buttonBack);
    buttonBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
  }

  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    switch (keyCode) {
    case KeyEvent.KEYCODE_MENU:
      break;
    case KeyEvent.KEYCODE_BACK:
      if (mProgressDialog != null) {
        mProgressDialog.dismiss();
        mRequestPost.cancel(true);
        mUIHandler.removeMessages(HANDLER_POST_FACEBOOK);
      }
      return super.onKeyUp(keyCode, event);
    }
    return super.onKeyUp(keyCode, event);
  }

}
