package com.bomzaiya.museumpost;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements
    SurfaceHolder.Callback {
  private static final String TAG = "CAMERAPREVIEW";
  private SurfaceHolder mHolder;
  private Camera mCamera;
  private LiveCameraActivity mCameraActivity;

  public CameraPreview(Context context) {
    super(context);
  }

  @SuppressLint("NewApi")
  @SuppressWarnings("deprecation")
  public CameraPreview(LiveCameraActivity activity, Camera camera) {
    super(activity.getBaseContext());
    mCamera = camera;

    LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
        LayoutParams.MATCH_PARENT);
    this.setLayoutParams(params);
    mCameraActivity = activity;
    // Install a SurfaceHolder.Callback so we get notified when the
    // underlying surface is created and destroyed.
    mHolder = getHolder();
    mHolder.addCallback(this);
    // deprecated setting, but required on Android versions prior to 3.0
    mHolder.setType(SurfaceHolder.SURFACE_TYPE_HARDWARE);
  }

  @SuppressLint("NewApi")
  public void surfaceCreated(SurfaceHolder holder) {
    // The Surface has been created, now tell the camera where to draw the
    // preview.
    boolean tablet = mCameraActivity.mApplication.isTablet();
    try {
      mCamera.setPreviewDisplay(mHolder);

      Camera.Parameters parameters = mCamera.getParameters();
//      List<Size> sizes = parameters.getSupportedPictureSizes();
//      for (int i = 0; i < sizes.size(); i++) {
//        Log.d("size_pic_support", sizes.get(i).width + ":"
//            + sizes.get(i).height);
//      }

      if (tablet) {
        parameters.setPictureSize(640, 480);
      } else {
        parameters.setPictureSize(720, 960);
      }

//      List<Size> size = parameters.getSupportedPreviewSizes();
//      for (int i = 0; i < size.size(); i++) {
//        Log.d("size_support", size.get(i).width + ":" + size.get(i).height);
//      }

      int result = 0;
      int angle = 0;
      if (tablet) {
        result = 90;
      } else {
        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(mCameraActivity.mCamId, info);
        result = (info.orientation + angle) % 360;
        result = (360 - result) % 360; // compensate the mirror
      }

      if (tablet) {
        parameters.setPreviewSize(640, 480);
      } else {
        parameters.setPreviewSize(720, 960);
      }
      mCamera.setParameters(parameters);
      mCamera.setDisplayOrientation(result);
      requestLayout();

      mCamera.startPreview();
    } catch (RuntimeException e) {
    } catch (IOException e) {
      Log.d(TAG, "Error setting camera preview: " + e.getMessage());
    }
  }

  public void setCamera(Camera camera) {
    mCamera = camera;
  }

  public void surfaceDestroyed(SurfaceHolder holder) {
    // empty. Take care of releasing the Camera preview in your activity.
  }

  public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
    // If your preview can change or rotate, take care of those events here.
    // Make sure to stop the preview before resizing or reformatting it.

    if (mHolder.getSurface() == null) {
      // // preview surface does not exist
      return;
    }

    // stop preview before making changes
    try {
      mCamera.stopPreview();
    } catch (Exception e) {
      // ignore: tried to stop a non-existent preview
    }

    // start preview with new settings
    try {
      // mCamera.setPreviewDisplay(mHolder);
      mCamera.startPreview();
      //
    } catch (Exception e) {
      // Log.d(TAG, "Error starting camera preview: " + e.getMessage());
    }
  }
}