package com.bomzaiya.museumpost;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bomzaiya.museumpost.media.OnFacebook;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;

public class PreloadActivity extends BomActivity {
  public static final String MUSEUM_FOLDER = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/nidamuseum";
  public static final String FRAME_FOLDER = MUSEUM_FOLDER + "/frame";
  private Button mButtonEnglish;
  private Button mButtonThai;
  private ProgressBar mPbLoading;
  private Button mButtonRetry;

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
      case InstructionActivity.MAIN_INSTRUCTION:
        String backTo = data.getStringExtra(BACK_TO);
        if (!getClass().getName().equals(backTo)) {
          setResult(Activity.RESULT_OK, data);
          finish();
        }
        break;

      default:
        break;
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  private MyHandler mUIHandler = new MyHandler(this);
  private TextView mTvMessage;

  protected static final int HANDLER_LOGIN_FACEBOOK = 1;

  protected static final int FACEBOOK_LOGIN_TIMEOUT = 20000; // 10 secs.

  private static class MyHandler extends Handler {
    private WeakReference<PreloadActivity> mActivity = null;

    public MyHandler(PreloadActivity activity) {
      mActivity = new WeakReference<PreloadActivity>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
      final PreloadActivity activity = mActivity.get();
      if (activity != null) {

        switch (msg.what) {
        case HANDLER_LOGIN_FACEBOOK:
          activity.showMessage(activity.getString(R.string.connection_timeout));
          activity.retry();
          break;
        }
      }
    }
  }

  private void retry() {
    mButtonRetry.setVisibility(Button.VISIBLE);
    mPbLoading.setVisibility(ProgressBar.GONE);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.preload);

    mPbLoading = (ProgressBar) findViewById(R.id.pbLoading);

    mButtonEnglish = (Button) findViewById(R.id.buttonEnglish);
    mButtonEnglish.setVisibility(Button.GONE);
    mButtonEnglish.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        mApplication.changeLocale("en");
        InstructionActivity.startForResult(PreloadActivity.this);
      }
    });

    mButtonThai = (Button) findViewById(R.id.buttonThai);
    mButtonThai.setVisibility(Button.GONE);
    mButtonThai.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        mApplication.changeLocale("th");
        InstructionActivity.startForResult(PreloadActivity.this);
      }
    });

    mButtonRetry = (Button) findViewById(R.id.buttonRetry);
    mButtonRetry.setVisibility(Button.GONE);
    mButtonRetry.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        signInWithFacebook();
      }
    });

    mTvMessage = (TextView) findViewById(R.id.tvMessage);
    hideMessage();

    copyFrame();

    copyDatabase();
    // Log.i("FACEBOOK_HASH", getFacebookKeyHash());

    saveHash(getFacebookKeyHash());

    signInWithFacebook();
  }

  private void copyDatabase() {
    // will copy db
    StatDatabaseAdapter db = new StatDatabaseAdapter(mApplication);
    db.close();
  }

  public void saveHash(String text) {
    File logFile = new File(FRAME_FOLDER, "hash.txt");

    String log = text;

    if (!logFile.exists()) {
      try {
        logFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    try {
      // BufferedWriter for performance, true to set append to file flag
      BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true), 8096);
      buf.append(log);
      buf.newLine();
      buf.flush();
      buf.close();
    } catch (IOException e) {
    }
  }

  public String getFacebookKeyHash() {
    try {
      PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
      for (Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        String hash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
        return hash;
      }
    } catch (NameNotFoundException e) {

    } catch (NoSuchAlgorithmException e) {

    }

    return "";
  }

  private void showMessage(final String message) {
    PreloadActivity.this.runOnUiThread(new Runnable() {
      @Override
      public void run() {
        mTvMessage.setText(message);
        mTvMessage.setVisibility(TextView.VISIBLE);
      }
    });
  }

  private void hideMessage() {
    mUIHandler.removeMessages(HANDLER_LOGIN_FACEBOOK);
    PreloadActivity.this.runOnUiThread(new Runnable() {

      @Override
      public void run() {
        mTvMessage.setText("");
        mTvMessage.setVisibility(TextView.GONE);
      }
    });
  }

  private void signInWithFacebook() {
    if (!mApplication.hasInternet()) {
      showMessage(getString(R.string.connection_error));
      retry();
      return;
    }

    mUIHandler.removeMessages(HANDLER_LOGIN_FACEBOOK);
    Message msg = mUIHandler.obtainMessage(HANDLER_LOGIN_FACEBOOK);
    mUIHandler.sendMessageDelayed(msg, FACEBOOK_LOGIN_TIMEOUT);

    mApplication.getFacebookPublishSession(this, new OnFacebook() {

      @Override
      public void onSuccess(Session session) {
        Bundle getParams = new Bundle();
        getParams.putString("fields", "access_token");
        String access_token_link = ProductConfig.FACEBOOK_ACCESS_TOKEN_LINK;
        // String access_token_link =
        // ProductConfig.FACEBOOK_PAGE_ACCESS_TOKEN_LINK;
        Request request = new Request(session, access_token_link, getParams, HttpMethod.GET, new Request.Callback() {

          @Override
          public void onCompleted(Response response) {
            if (response.getGraphObject() != null) {
              JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
              mApplication.setPageAccessToken("");
              try {
                String pageAccessToken = graphResponse.getString("access_token");
                mApplication.setPageAccessToken(pageAccessToken);

                hideMessage();
                mButtonEnglish.setVisibility(Button.VISIBLE);
                mButtonThai.setVisibility(Button.VISIBLE);
                mPbLoading.setVisibility(ProgressBar.GONE);
                mButtonRetry.setVisibility(Button.GONE);
              } catch (JSONException e) {
                Log.i("XXX", "JSON error " + e.getMessage());
              }
            }
          }

        });

        RequestAsyncTask task = new RequestAsyncTask(request);
        task.execute();
      }
    });
  }

  private void copyFrame() {
    File mediaStorageDir = new File(MUSEUM_FOLDER);
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.

    // Create the storage directory if it does not exist
    if (!mediaStorageDir.exists()) {
      if (!mediaStorageDir.mkdirs()) {
        // Log.d("MyCameraApp", "failed to create directory");
      }
    }

    File frameStorageDir = new File(FRAME_FOLDER);
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.

    if (!frameStorageDir.exists()) {
      if (!frameStorageDir.mkdirs()) {
        // Log.d("MyCameraApp", "failed to create directory");
      } else {
        copyFrameAssets();
      }
    } else {
      copyFrameAssets();
    }
  }

  private void copyFrameAssets() {
    AssetManager assetManager = getAssets();
    String[] files = null;
    try {
      files = assetManager.list("frame");
    } catch (IOException e) {
      Log.e("tag", "Failed to get asset file list.", e);
    }
    for (String filename : files) {
      InputStream in = null;
      OutputStream out = null;
      try {
        in = assetManager.open("frame/" + filename);

        File fileOutput = new File(FRAME_FOLDER + "/", filename);

        if (!fileOutput.exists()) {
          out = new FileOutputStream(fileOutput);
          copyFile(in, out);
          in.close();
          in = null;
          out.flush();
          out.close();
          out = null;
        }
      } catch (IOException e) {
        Log.e("tag", "Failed to copy asset file: " + filename, e);
      }
    }
  }

  private void copyFile(InputStream in, OutputStream out) throws IOException {
    byte[] buffer = new byte[1024];
    int read;
    while ((read = in.read(buffer)) != -1) {
      out.write(buffer, 0, read);
    }
  }

}
