package com.bomzaiya.museumpost;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.bomzaiya.data.DatabaseAdapter;

@SuppressLint("DefaultLocale")
public class StatDatabaseAdapter extends DatabaseAdapter {
  public static final String TABLE_STATS = "stats";

  public static final String STATS_COLUMN_ID = "id";
  public static final String STATS_COLUMN_LIKE = "like";
  public static final String STATS_COLUMN_DISLIKE = "dislike";
  public static final String STATS_COLUMN_FRAME_ONE = "frame_one";
  public static final String STATS_COLUMN_FRAME_TWO = "frame_two";
  public static final String STATS_COLUMN_FRAME_THREE = "frame_three";
  public static final String STATS_COLUMN_FRAME_FOUR = "frame_four";
  public static final String STATS_COLUMN_STAT_DATE = "stat_date";
  
  public static final String STATS_COLUME_LIKE_COUNT = "likeCount";
  public static final String STATS_COLUME_DISLIKE_COUNT = "dislikeCount";
  
  
  public StatDatabaseAdapter(Context ctx) {
    super(ctx, "nidamuseum.sqlite");
  }

  public Cursor getDateStats() {
    try {
      String criteria = "";

      db = getDatabaseHelper().getReadableDatabase();
      return db.query(TABLE_STATS, new String[] { STATS_COLUMN_ID, STATS_COLUMN_LIKE, STATS_COLUMN_DISLIKE, STATS_COLUMN_FRAME_ONE,
          STATS_COLUMN_FRAME_TWO, STATS_COLUMN_FRAME_THREE, STATS_COLUMN_FRAME_FOUR, STATS_COLUMN_STAT_DATE }, criteria, null, null, null,
          null, null);
    } catch (SQLiteException e) {
      e.printStackTrace();
    }

    return null;
  }

  public Cursor getLikeDislikeCount() {
    try {
      db = getDatabaseHelper().getReadableDatabase();

      StringBuilder query = new StringBuilder();

      query.append("SELECT ").append("sum(" + STATS_COLUMN_LIKE + ") as " + STATS_COLUME_LIKE_COUNT)
           .append(", sum(" + STATS_COLUMN_DISLIKE + ") as " + STATS_COLUME_DISLIKE_COUNT)
           .append(" FROM " + TABLE_STATS);

      return db.rawQuery(query.toString(), null);
    } catch (SQLiteException e) {
//      e.printStackTrace();
    }

    return null;
  }

  private String getTodayDate() {
    Calendar cal = Calendar.getInstance();
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);
    int day = cal.get(Calendar.DAY_OF_MONTH);

    String monthString = String.format("%02d", month);
    String dayString = String.format("%02d", day);
    String dateString = year + "-" + monthString + "-" + dayString;
    return dateString;
  }

  public int getTodayStatId() {
    try {
      String dateString = DatabaseUtils.sqlEscapeString(getTodayDate());

      String criteria = "";

      criteria = STATS_COLUMN_STAT_DATE + " = " + dateString;

      SQLiteDatabase db = getDatabaseHelper().getReadableDatabase();
      Cursor cursor = db.query(TABLE_STATS, new String[] { STATS_COLUMN_ID }, criteria, null, null, null, null, null);
      if (cursor != null) {
        if (cursor.getCount() > 0) {
          cursor.moveToFirst();
          int id = cursor.getInt(cursor.getColumnIndexOrThrow(STATS_COLUMN_ID));
          cursor.close();
          db.close();
          return id;
        }
      }
      db.close();

    } catch (SQLiteException e) {
      // e.printStackTrace();
    }

    return 0;
  }

  public boolean updateFrameRecord(int frame_id) {
    String frame_field = "";
    int frame_1_value = 0;
    int frame_2_value = 0;
    int frame_3_value = 0;
    int frame_4_value = 0;
    switch (frame_id) {
    case 1:
      frame_1_value = 1;
      frame_field = STATS_COLUMN_FRAME_ONE;
      break;
    case 2:
      frame_2_value = 1;
      frame_field = STATS_COLUMN_FRAME_TWO;
      break;
    case 3:
      frame_3_value = 1;
      frame_field = STATS_COLUMN_FRAME_THREE;
      break;
    case 4:
      frame_4_value = 1;
      frame_field = STATS_COLUMN_FRAME_FOUR;
    default:
    }

    SQLiteDatabase db = getDatabaseHelper().getWritableDatabase();
    try {
      StringBuilder query = new StringBuilder();

      int id = getTodayStatId();
      if (id > 0) {
        query.append("UPDATE ").append(TABLE_STATS).append(" SET ").append(frame_field).append(" = ")
            .append("ifnull(" + frame_field + ",0) + 1").append(" WHERE ").append(STATS_COLUMN_ID).append(" = ").append(id);
      } else {
        String dateString = DatabaseUtils.sqlEscapeString(getTodayDate());
        query.append("INSERT INTO ").append(TABLE_STATS).append("(").append(STATS_COLUMN_FRAME_ONE).append(",")
            .append(STATS_COLUMN_FRAME_TWO).append(",").append(STATS_COLUMN_FRAME_THREE).append(",").append(STATS_COLUMN_FRAME_FOUR)
            .append(",").append(STATS_COLUMN_STAT_DATE).append(") ").append(" VALUES(").append(frame_1_value).append(",")
            .append(frame_2_value).append(",").append(frame_3_value).append(",").append(frame_4_value).append(",").append(dateString)
            .append(") ");
      }
      db.execSQL(query.toString());
      db.close();
      return true;
    } catch (NullPointerException e) {
    } catch (SQLiteException e) {
      // e.printStackTrace();
    } catch (IllegalStateException e) {
    } finally {
      try {
        db.close();
      } catch (Exception e) {
      }
    }
    return false;
  }

  public boolean updateLikeRecord(boolean like) {

    String like_field = "";
    int like_value = 0;
    int dislike_value = 0;
    if (like) {
      like_value = 1;
      like_field = STATS_COLUMN_LIKE;
    } else {
      dislike_value = 1;
      like_field = STATS_COLUMN_DISLIKE;
    }

    SQLiteDatabase db = getDatabaseHelper().getWritableDatabase();
    try {
      StringBuilder query = new StringBuilder();

      int id = getTodayStatId();
      if (id > 0) {
        query.append("UPDATE ").append(TABLE_STATS).append(" SET ").append(like_field).append(" = ")
            .append("ifnull(" + like_field + ",0) + 1").append(" WHERE ").append(STATS_COLUMN_ID).append(" = ").append(id);
      } else {
        String dateString = DatabaseUtils.sqlEscapeString(getTodayDate());
        query.append("INSERT INTO ").append(TABLE_STATS).append("(").append(STATS_COLUMN_LIKE).append(",").append(STATS_COLUMN_DISLIKE)
            .append(",").append(STATS_COLUMN_STAT_DATE).append(") ").append(" VALUES(").append(like_value).append(",")
            .append(dislike_value).append(",").append(dateString).append(") ");
      }
      db.execSQL(query.toString());
      db.close();
      return true;
    } catch (NullPointerException e) {
    } catch (SQLiteException e) {
      // e.printStackTrace();
    } catch (IllegalStateException e) {
    } finally {
      try {
        db.close();
      } catch (Exception e) {
      }
    }

    return false;
  }
}