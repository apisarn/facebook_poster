package com.bomzaiya.museumpost.media;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class MediaUtility {
  public static Bitmap readBitmap(Context context, String path) {
    try {
      File file = new File(path);

      InputStream is = new FileInputStream(file);
      return BitmapFactory.decodeStream(is, null, null);
    } catch (IOException e) {
    }
    return null;
  }

  public static Drawable readDrawable(Context context, String path) {
    Bitmap bi = readBitmap(context, path);

    Drawable drawable = null;
    drawable = new BitmapDrawable(context.getResources(), bi);
    return drawable;
  }

}
