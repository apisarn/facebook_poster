package com.bomzaiya.museumpost;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LikeActivity extends BomActivity {
  protected static final int LIKE_MUSEUM = 200;
  protected static final int LIKE = 1;
  protected static final int NOTLIKE = 2;

  public static void startForResult(BomActivity activity) {
    Intent intent = new Intent(activity, LikeActivity.class);
    activity.startActivityForResult(intent, LIKE_MUSEUM);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      switch (requestCode) {
      case ChooseFrameActivity.CHOOSE_FRAME:
        String backTo = data.getStringExtra(BACK_TO);
        if (!getClass().getName().equals(backTo)) {
          setResult(Activity.RESULT_OK, data);
          finish();
        }
        break;

      default:
        break;
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    // outState.put
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.like);

    Button btnLike = (Button) findViewById(R.id.buttonLike);
    Button btnNotLike = (Button) findViewById(R.id.buttonNotLike);

    btnLike.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        saveLike(LIKE);
        ChooseFrameActivity.startForResult(LikeActivity.this);
      }

    });

    btnNotLike.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        saveLike(NOTLIKE);
        ChooseFrameActivity.startForResult(LikeActivity.this);
      }
    });

    Button buttonBack = (Button) findViewById(R.id.buttonBack);
    buttonBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
      }
    });
  }

  private void saveLike(int like) {
    // save current date
    StatDatabaseAdapter db = new StatDatabaseAdapter(mApplication);
    // notlike++
    if (like == LIKE) {
      // like++
      db.updateLikeRecord(true);
    } else {
      // notlike++
      db.updateLikeRecord(false);
    }
    db.close();
  }
}
