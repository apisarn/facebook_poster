<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:android='http://schemas.android.com/apk/res/android'>
	<xsl:param name="PRODUCTCONFIG.PACKAGE_NAME" />
	<xsl:param name="PRODUCTCONFIG.VERSION_CODE" />
	<xsl:param name="PRODUCTCONFIG.VERSION_NAME" />
	<xsl:param name="PRODUCTCONFIG.APP_ICON" />
	<xsl:param name="PRODUCTCONFIG.APP_NAME" />
	<!-- <xsl:param name="PRODUCTCONFIG.FACEBOOK_APP_ID" /> -->
	<xsl:output indent="no" />

	<!-- Supplies a newline after the XML header. It's cosmetic, but something 
		to include in each script. -->
	<xsl:template match="/">
		<xsl:text>
		</xsl:text>
		<xsl:apply-templates select="@*|node()" />
	</xsl:template>

	<xsl:template match="/manifest/@android:versionName">
		<xsl:attribute name="android:versionName"><xsl:value-of
			select="$PRODUCTCONFIG.VERSION_NAME" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="/manifest/@android:versionCode">
		<xsl:attribute name="android:versionCode"><xsl:value-of
			select="$PRODUCTCONFIG.VERSION_CODE" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="/manifest/@package">
		<xsl:attribute name="package"><xsl:value-of
			select="$PRODUCTCONFIG.PACKAGE_NAME" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="/manifest/application/@android:icon">
		<xsl:attribute name="android:icon"><xsl:value-of
			select="$PRODUCTCONFIG.APP_ICON" /></xsl:attribute>
	</xsl:template>

	<xsl:template match="/manifest/application/@android:label">
		<xsl:attribute name="android:label"><xsl:value-of
			select="$PRODUCTCONFIG.APP_NAME" /></xsl:attribute>
	</xsl:template>

	<!-- <xsl:template -->
	<!-- match="meta-data[@android:name='com.facebook.sdk.ApplicationId']"> -->
	<!-- <xsl:copy> -->
	<!-- <xsl:apply-templates select="@*|node()" /> -->
	<!-- <xsl:attribute name="android:value"><xsl:value-of -->
	<!-- select="$PRODUCTCONFIG.FACEBOOK_APP_ID" /></xsl:attribute> -->
	<!-- </xsl:copy> -->
	<!-- </xsl:template> -->

	<!-- Identity transform by default. Generally, you'll put this in each script 
		you write, if you're just making changes. It copies everything unchanged 
		that doesn't have its own template. -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
